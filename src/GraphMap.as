/**
 * Created by oleg on 3/29/14.
 */
package {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;
import flash.utils.Dictionary;

import ru.mymindspam.rzdmap.utils.XMLParser;

[SWF(width='1000', height='600', backgroundColor='#ffffff', frameRate='30')]
public class GraphMap extends Sprite{
    [Embed(source="../assets/map.jpg")]
    private const MAP_IMAGE:Class;

    [Embed(source='../assets/_TestGraph.xml',
            mimeType="application/octet-stream")]
    private const TEST_GRAPH_XML:Class;
    private var xmlParser:XMLParser;

    private var mapImage:Bitmap;
    private var scrollableContainer:Sprite;
    private var graphs:Vector.<Graph>;
    private var layers:Dictionary;


    private var layerPanel:LayerPanel;
    //scrolling
    private const VELOSITY:Number = 0.3;
    private var mouseDown:Boolean = false;
    private var mouseDownPoint:Point = new Point();
    private var thisMouseDownPoint:Point = new Point();
    private var minX:Number;
    private var minY:Number;
    private var scaleMin:Number;
    private var scaleMax:Number;
    private var viewPort:Rectangle;
    private var mouseDownCounter:int = 0;
    public function GraphMap() {
        trace('Start');
        initMap();
        initContainers();
        graphs = new Vector.<Graph>();
        layers = new Dictionary();

        initLayerPanel();
        //mouse
        viewPort = new Rectangle(0,0,1000,600);
        minX = -(scrollableContainer.width-viewPort.width);
        minY = -(scrollableContainer.height-viewPort.height);
        scaleMin = 0.006;
        scaleMax = 2;
        //TODO: scaleMin = Math.max();
        this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
        this.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);

        this.addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelHandler);

        //test data
        xmlParser = new XMLParser();
        var ribs:Vector.<Rib> = xmlParser.getRibs(new XML(new TEST_GRAPH_XML()));
        addGraph('Huge', 0x0000FF, ribs);
        //addTestData('Test',0xFF0000);
        //addTestData('Test2',0x000000);
        //selectPath();
    }

    /*public function selectPath(path:String):void{
        var array:Array = path.split(";");
        var id:String;
        var n:uint = array.length;
        for(var i:uint=0;i<n;i++){
            points[array[i]].selected = true;
        }
    }*/

    public function addGraph(name:String, color:uint, ribs:Vector.<Rib>):void{
        var graph:Graph = new Graph(name, color, ribs);
        graphs.push(graph);
        drawGraph(graph);
        layerPanel.addLayer(name);
    }

    private function initLayerPanel():void {
        layerPanel = new LayerPanel();
        layerPanel.addEventListener(Event.CHANGE, layerPanelChangeHandler)
        addChild(layerPanel);
    }

    private function layerPanelChangeHandler(event:Event):void {
        var item:Sprite = layers[layerPanel.selectedItemId];
        item.visible = !item.visible;
    }

    private function mouseWheelHandler(event:MouseEvent):void {

        var oldWidth:Number = scrollableContainer.width;
        var oldHeight:Number = scrollableContainer.height;
        var newScale:Number = scrollableContainer.scaleX + event.delta*0.01;
        if(newScale>scaleMax)
            newScale = scaleMax;
        if(newScale<scaleMin)
            newScale = scaleMin;
        scrollableContainer.scaleX = scrollableContainer.scaleY = newScale;
        scrollableContainer.x = int(((2 * mouseX) - (2 * (event.localX * scrollableContainer.scaleX))) * 0.5);
        scrollableContainer.y = int(((2 * mouseY) - (2 * (event.localY * scrollableContainer.scaleY))) * 0.5);
        //scrollableContainer.x += event.stageX+(oldWidth - scrollableContainer.width)*0.5;
        //scrollableContainer.y += event.stageY+(oldHeight - scrollableContainer.height)*0.5;

        /*TODO:remove
        if(scrollableContainer.width<viewPort.width){
            scrollableContainer.width = viewPort.width;
            scrollableContainer.scaleY = scrollableContainer.scaleX;
        }
        if(scrollableContainer.height<viewPort.height){
            scrollableContainer.height = viewPort.height;
            scrollableContainer.scaleX = scrollableContainer.scaleY;
        }
        trace('scrollableContainer.scaleX',scrollableContainer.scaleX);*/

        //TODO: double usage
        minX = -(scrollableContainer.width-viewPort.width);
        minY = -(scrollableContainer.height-viewPort.height);
        if(scrollableContainer.x > 0)
            scrollableContainer.x = 0;
        else if(scrollableContainer.x < minX)
            scrollableContainer.x = minX;

        if(scrollableContainer.y > 0)
            scrollableContainer.y = 0;
        else if(scrollableContainer.y < minY)
            scrollableContainer.y = minY;
    }

    private function mouseUpHandler(event:MouseEvent):void {
        mouseDown = false;
        stage.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
        for each(var drawnGraph:DrawnGraph in layers){
            drawnGraph.showPoints(false);
        }
    }

    private function mouseDownHandler(event:MouseEvent):void {
        mouseDown = true;
        mouseDownCounter = 2;
        stage.addEventListener(Event.ENTER_FRAME, enterFrameHandler);
        for each(var drawnGraph:DrawnGraph in layers){
            drawnGraph.showPoints();
        }
        mouseDownPoint.x = event.stageX;
        mouseDownPoint.y = event.stageY;
        thisMouseDownPoint.x = scrollableContainer.x;
        thisMouseDownPoint.y = scrollableContainer.y;
    }

    private function enterFrameHandler(event:Event):void {
        if(mouseDown) {
            
            if(mouseDownCounter>0) {
                mouseDownCounter--;
                if(mouseDownCounter==1){
                    for each(var drawnGraph:DrawnGraph in layers){
                        drawnGraph.showPoints(false);
                    }
                }
            }else{
                scrollableContainer.x = int(thisMouseDownPoint.x + (stage.mouseX - mouseDownPoint.x));
                scrollableContainer.y = int(thisMouseDownPoint.y + (stage.mouseY - mouseDownPoint.y));
            }
        }

        /*if(scrollableContainer.x > 0)
            scrollableContainer.x = 0;
        else if(scrollableContainer.x < minX)
            scrollableContainer.x = minX;

        if(scrollableContainer.y > 0)
            scrollableContainer.y = 0;
        else if(scrollableContainer.y < minY)
            scrollableContainer.y = minY;*/
    }

    private function addTestData(name:String, color):void {
        var ribs:Vector.<Rib>;
        var n:uint = 10;
        var startX:uint = 200+Math.random()*100;
        var startY:uint = 200+Math.random()*100;
        var endX:uint;
        var endY:uint;
        var rib:Rib;
        ribs = new Vector.<Rib>();
        for(var i:uint=0;i<n;i++){
            endX = 200+Math.random()*300;
            endY = 200+Math.random()*300;
            rib = new Rib(new Point(startX,startY),new Point(endX,endY));
            ribs.push(rib);
            startX = endX;
            startY = endY;
        }
        addGraph(name, color, ribs);
    }

    private function drawGraph(graph:Graph):void {
        var drawnGraph:DrawnGraph = new DrawnGraph(graph);
        layers[graph.name] = drawnGraph;
        //sprite.addChild(pointsContainer);
        drawnGraph.showPoints(false);
        scrollableContainer.addChild(drawnGraph);

    }


    private function initMap():void {
        var bitmapData:BitmapData = new BitmapData(4000,4000,false,0x808080);
        mapImage = new MAP_IMAGE() as Bitmap;
        bitmapData.draw(mapImage);
        mapImage = new Bitmap(bitmapData);
    }

    private function initContainers():void {
        scrollableContainer = new Sprite();
        addChild(scrollableContainer);
        scrollableContainer.addChild(mapImage);

    }
}
}
