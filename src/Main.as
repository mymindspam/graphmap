/**
 * Created by oleg on 4/25/14.
 */
package {
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.geom.Rectangle;

import ru.mymindspam.rzdmap.ScrollableMap;

import ru.mymindspam.rzdmap.components.itemsPanel.ItemsPanel;
import ru.mymindspam.rzdmap.model.Graph;
import ru.mymindspam.rzdmap.model.Rib;
import ru.mymindspam.rzdmap.utils.XMLParser;

[SWF(width='1000', height='600', backgroundColor='#ffffff', frameRate='30')]
public class Main extends Sprite{
    private var xmlParser:XMLParser;

    private var scrollableMap:ScrollableMap;
    private var itemsPanel:ItemsPanel;

    public function Main() {
        trace("START");
        initMap();
        initItemsPanel();
        //foo data
        xmlParser = new XMLParser();
        var ribs:Vector.<Rib> = xmlParser.getRibs(new XML(new EmbededResourses.TEST_GRAPH_XML()));
        scrollableMap.addGraph(new Graph('Huge',ribs,0xFF0000));
        //scrollableMap.selectPointOfGraphByID('Huge',"10-20");
    }

    private function initMap():void {
        scrollableMap = new ScrollableMap();
        addChild(scrollableMap);
    }

    private function initItemsPanel():void {
        itemsPanel = new ItemsPanel();
        addChild(itemsPanel);
    }
}
}
