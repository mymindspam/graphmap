/**
 * Created by oleg on 4/25/14.
 */
package {
public class EmbededResourses {
    [Embed(source="../assets/map.jpg")]
    public static const MAP_IMAGE:Class;

    [Embed(source='../assets/TestGraph.xml',
            mimeType="application/octet-stream")]
    public static const TEST_GRAPH_XML:Class;

    [Embed(source="../assets/checkButton/off.png")]
    public static const CHECK_BUTTON_OFF_IMAGE:Class;

    [Embed(source="../assets/checkButton/on.png")]
    public static const CHECK_BUTTON_ON_IMAGE:Class;

    [Embed(source="../assets/point.png")]
    public static const POINT_IMAGE:Class;
}
}
