/**
 * Created by oleg on 3/29/14.
 */
package {

import flash.display.Bitmap;
import flash.display.Sprite;

public class DrawnPoint extends Sprite {
    [Embed(source='../assets/point.png')]
    private static const POINT_IMAGE:Class;
    private var pointImage:Bitmap;

    private const RADIUS:Number = 5;
    private var _selected:Boolean = false;
    public function DrawnPoint() {
        init();
    }

    public function get selected():Boolean{
        return _selected;
    }

    public function set selected(value:Boolean):void{
        _selected = value;
        if(_selected) this.scaleX = this.scaleY = 2;
        else this.scaleX = this.scaleY = 1;
    }

    private function init():void {
        pointImage = new POINT_IMAGE();
        pointImage.x = -pointImage.width*0.5;
        pointImage.y = -pointImage.height*0.5;
        addChild(pointImage);
    }
}
}
