/**
 * Created by oleg on 3/30/14.
 */
package {
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.text.TextField;
import flash.text.TextFieldAutoSize;

public class LayerPanel extends Sprite {
    private var textFields:Vector.<TextField>;
    public var selectedItemId:String;
    public function LayerPanel() {
        textFields = new Vector.<TextField>();
        initBG();
    }

    private function initBG():void {
        graphics.beginFill(0xF2F4F5);
        graphics.drawRect(0,0,300,40);
    }

    public function addLayer(id:String):void{
        var textField:TextField = new TextField();
        textField.text = id;
        textField.border = true;
        textField.selectable = false;
        textField.autoSize = TextFieldAutoSize.LEFT;
        addChild(textField);
        textFields.push(textField);
        textField.x = 100*textFields.length;
        textField.y = 40*0.5 - textField.height*0.5;
        textField.addEventListener(MouseEvent.CLICK, selectItemHandler)
    }

    private function selectItemHandler(event:MouseEvent):void {
        //TODO: text as id
        if(event.target.alpha == 1) event.target.alpha = 0.5;
        else event.target.alpha = 1;
        selectedItemId = (event.target as TextField).text;
        dispatchEvent(new Event(Event.CHANGE));
    }
}
}
