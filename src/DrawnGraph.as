/**
 * Created by oleg on 4/1/14.
 */
package {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.events.MouseEvent;
import flash.utils.Dictionary;

public class DrawnGraph extends Sprite{
    private var points:Dictionary;
    private var pointsContainer:Sprite;
    private var selectedPoint:DrawnPoint;
    public function DrawnGraph(graph:Graph) {
        init(graph);
    }

    public function showPoints(value:Boolean = true):void{
        pointsContainer.visible = value;
    }

    private function init(graph:Graph):void {
        points = new Dictionary();
        pointsContainer = new Sprite();
        var sprite:Sprite = new Sprite();
        var ribs:Vector.<Rib> = graph.ribs;

        var graphics:Graphics = sprite.graphics;
        graphics.lineStyle(4,graph.color);
        var n:uint = graph.ribs.length;
        var rib:Rib;
        var startPointId:String;
        var endPointId:String;

        for(var i:uint = 0;i<n;i++){
            rib = ribs[i];
            startPointId = String(rib.startPoint.x)+String(rib.startPoint.y);
            endPointId = String(rib.endPoint.x)+String(rib.endPoint.y);
            addPoint(pointsContainer, startPointId, rib.startPoint.x, rib.startPoint.y);
            addPoint(pointsContainer, endPointId, rib.endPoint.x, rib.endPoint.y);
            graphics.moveTo(rib.startPoint.x,rib.startPoint.y);
            graphics.lineTo(rib.endPoint.x,rib.endPoint.y);
        }
        var bitmapData:BitmapData = new BitmapData(sprite.width,sprite.height,true,0x00000000);
        bitmapData.draw(sprite);
        bitmapData.draw(pointsContainer);
        graphics.clear();
        addChild(new Bitmap(bitmapData));
        addChild(pointsContainer);
    }

    private function addPoint(layer:Sprite, id:String, x:Number, y:Number):void {
        if(points.hasOwnProperty(id)) return;
        var drawnPoint:DrawnPoint = new DrawnPoint();
        drawnPoint.x = x;
        drawnPoint.y = y;
        drawnPoint.addEventListener(MouseEvent.CLICK, drawnPointClickHandler)
        points[id] = drawnPoint;
        layer.addChild(drawnPoint);
    }

    private function drawnPointClickHandler(event:MouseEvent):void {
        if(selectedPoint)
            selectedPoint.selected = false;
        if(event.target == selectedPoint){
            selectedPoint = null;
            return;
        }
        selectedPoint = event.target as DrawnPoint;
        selectedPoint.selected = true;
    }
}
}
