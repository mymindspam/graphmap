/**
 * Created by oleg on 4/1/14.
 */
package ru.mymindspam.rzdmap.utils {
import flash.geom.Point;

import ru.mymindspam.rzdmap.model.Rib;

public class XMLParser {
    public function XMLParser() {
    }
    public function getRibs(xml:XML):Vector.<Rib>{
        var ribs:Vector.<Rib> = new Vector.<Rib>();
        var xmlList:XMLList = xml.rib;
        var o:XML;
        var startPoint:Point;
        var endPoint:Point;
        for each (o in xmlList){
            startPoint = new Point(Number(o.x),Number(o.y));
            endPoint = new Point(Number(o.xEnd),Number(o.yEnd));
            ribs.push(new Rib(startPoint, endPoint));
        }
        return ribs;
    }
}
}
