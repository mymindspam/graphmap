/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap.components.itemsPanel {
import flash.display.Sprite;

public class ItemsPanel extends Sprite{
    private const ITEM_WIDTH:Number = 300;
    private var items:Vector.<ItemsPanelItem>;
    public function ItemsPanel() {
        init();
    }

    public function addItem(id:String):void{
        var item:ItemsPanelItem = new ItemsPanelItem(id);
        addChild(item);
        item.x = ITEM_WIDTH*items.length;
        items.push(item);
    }

    private function init():void {
        items = new Vector.<ItemsPanelItem>();
    }
}
}
