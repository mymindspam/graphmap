/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap.components.itemsPanel {
import flash.display.Sprite;
import flash.text.TextField;

public class ItemsPanelItem extends Sprite{
    public var id:String;
    private var label:TextField;
    private var checkButton:CheckButton;
    public function ItemsPanelItem(id:String) {
        init(id);
    }

    private function init(id:String):void {
        this.id = id;
        label = new TextField();
        addChild(label);
        checkButton = new CheckButton();
        addChild(checkButton);
        label.text = id;
        label.x = checkButton.width;
    }
}
}
