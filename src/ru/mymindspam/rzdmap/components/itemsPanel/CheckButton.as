/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap.components.itemsPanel {
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.Event;

public class CheckButton extends Sprite{
    private var onBitmap:Bitmap;
    private var offBitmap:Bitmap;
    public var isOn:Boolean = false;
    public function CheckButton() {
        init();
    }

    public function toggle():void{
        isOn = !isOn;
        onBitmap.visible = isOn;
        offBitmap.visible = !isOn;
        dispatchEvent(new Event(Event.CHANGE));
    }

    private function init():void {
        offBitmap = new EmbededResourses.CHECK_BUTTON_OFF_IMAGE();
        addChild(offBitmap);
        onBitmap = new EmbededResourses.CHECK_BUTTON_ON_IMAGE();
        addChild(onBitmap);
    }
}
}
