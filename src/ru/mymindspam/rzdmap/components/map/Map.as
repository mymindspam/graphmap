/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap.components.map {
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.utils.Dictionary;
import ru.mymindspam.rzdmap.model.Graph;

public class Map extends Sprite{
    private var backgroundBitmap:Bitmap;
    protected var graphSprites:Dictionary;
    public function Map() {
        initBackgroundBitmap();
    }

    public function addGraph(graph:Graph):void{
        if(!graphSprites)
            graphSprites = new Dictionary();
        var graphSprite:GraphSprite = new GraphSprite(graph);
        graphSprites[graph.id] = graphSprite;
        addChild(graphSprite);
    }

    public function showGraphSpriteByID(id:String, value:Boolean = true):void{
        graphSprites[id].visible = value;
    }

    public function selectPointOfGraphByID(graphID:String, id:String):void{
        var graphSprite:GraphSprite = graphSprites[graphID];
        graphSprite.selectPointByID(id);
    }

    private function initBackgroundBitmap():void {
        backgroundBitmap = new EmbededResourses.MAP_IMAGE();
        addChild(backgroundBitmap);
    }
}
}
