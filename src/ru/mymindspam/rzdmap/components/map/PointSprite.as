/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap.components.map {
import flash.display.Bitmap;
import flash.display.Sprite;
import flash.events.Event;

public class PointSprite extends Sprite{
    public var isOn:Boolean = false;
    private var onBitmap:Bitmap;
    private var offBitmap:Bitmap;
    public function PointSprite() {
        onBitmap = new EmbededResourses.POINT_IMAGE();
        onBitmap.scaleX = onBitmap.scaleY = 2;
        onBitmap.x = -onBitmap.width*0.5;
        onBitmap.y = -onBitmap.height*0.5;
        addChild(onBitmap);
        offBitmap = new EmbededResourses.POINT_IMAGE();
        offBitmap.x = -offBitmap.width*0.5;
        offBitmap.y = -offBitmap.height*0.5;
        addChild(offBitmap);
        select(false);
        //select(true);
    }

    public function select(value:Boolean = true):void{
        isOn = value;
        onBitmap.visible = isOn;
        offBitmap.visible = !isOn;
        dispatchEvent(new Event(Event.CHANGE));
    }
}
}
