/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap.components.map {
import flash.display.Bitmap;
import flash.display.BitmapData;
import flash.display.Graphics;
import flash.display.Sprite;
import flash.geom.Point;
import flash.utils.Dictionary;

import ru.mymindspam.rzdmap.components.map.PointSprite;

import ru.mymindspam.rzdmap.model.Graph;
import ru.mymindspam.rzdmap.model.Rib;

public class GraphSprite extends Sprite{
    private var points:Dictionary;
    private var graphContainer:Sprite;
    private var pointsContainer:Sprite;
    private var bitmapRibs:Bitmap;
    private var bitmapPoints:Bitmap;
    public function GraphSprite(value:Graph) {
        init(value);
    }

    public function selectPointByID(id:String):void{
        var pointSprite:PointSprite = points[id];
        pointSprite.select();
        redrawPoints();
    }

    private function init(value:Graph):void {
        points = new Dictionary();
        graphContainer = new Sprite();
        pointsContainer = new Sprite();
        draw(value);
    }

    private function draw(graph:Graph):void {
        var ribs:Vector.<Rib> = graph.ribs;
        var graphics:Graphics = graphContainer.graphics;
        graphics.lineStyle(4,graph.color);
        var n:uint = graph.ribs.length;
        var rib:Rib;

        var pointSprite:PointSprite;
        for(var i:uint = 0;i<n;i++){
            rib = ribs[i];
            //TODO: нормальные id для точек
            addPoint(rib.startPoint);
            //pointStartID = String(rib.startPoint.x)+String(rib.startPoint.y);
            //pointEndID =  String(rib.endPoint.x)+String(rib.endPoint.y);
            graphics.moveTo(rib.startPoint.x,rib.startPoint.y);
            graphics.lineTo(rib.endPoint.x,rib.endPoint.y);

        }
        redraw();

    }

    private function redraw():void {
        redrawRibs();
        redrawPoints();
    }

    private function redrawRibs():void {
        if(bitmapRibs)
            bitmapRibs.bitmapData.dispose();
        var bitmapRibsData:BitmapData = new BitmapData(graphContainer.width,graphContainer.height,true,0x00000000);
        bitmapRibsData.draw(graphContainer);
        bitmapRibs = new Bitmap(bitmapRibsData);
        addChild(bitmapRibs);

    }

    private function redrawPoints():void {
        if(bitmapPoints)
            bitmapPoints.bitmapData.dispose();
        var bitmapPointsData:BitmapData = new BitmapData(pointsContainer.width,pointsContainer.height,true,0x00000000);
        bitmapPointsData.draw(pointsContainer);
        bitmapPoints = new Bitmap(bitmapPointsData);
        addChild(bitmapPoints);
    }


    private function addPoint(point:Point):void{
        var id:String = String(point.x)+String(point.y);
        if(!points.hasOwnProperty(id)) {
            var pointSprite:PointSprite = new PointSprite();
            points[id] = pointSprite;
            pointSprite.x = point.x;
            pointSprite.y = point.y;
            pointsContainer.addChild(pointSprite);
        }
    }

    public function checkClosePoints(x:Number, y:Number):void {
        for each(var point:PointSprite in points){
            if((point.x-5<x && point.x+5>x) && (point.y-5<y && point.y+5>y)){
                point.select();
            }
        }
        redrawPoints();
    }
}
}
