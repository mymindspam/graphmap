/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap {
import flash.display.Sprite;
import flash.events.Event;
import flash.events.MouseEvent;
import flash.geom.Point;
import flash.geom.Rectangle;

import ru.mymindspam.rzdmap.components.map.GraphSprite;

import ru.mymindspam.rzdmap.components.map.Map;

public class ScrollableMap extends Map{
    //scrolling
    private var minX:Number;
    private var minY:Number;
    private var scaleMin:Number = 0.006;
    private var scaleMax:Number = 2;
    private var viewPort:Rectangle;
    private var mouseDown:Boolean = false;
    private var mouseDownPoint:Point;
    private var thisMouseDownPoint:Point;

    public function ScrollableMap() {
        initScroll();
    }

    private function initScroll():void {
        //min max x y
        mouseDownPoint = new Point();
        thisMouseDownPoint = new Point();
        viewPort = new Rectangle(0,0,1000,600);
        minX = -(this.width-viewPort.width);
        minY = -(this.height-viewPort.height);
        this.addEventListener(MouseEvent.MOUSE_DOWN, mouseDownHandler);
        this.addEventListener(MouseEvent.MOUSE_UP, mouseUpHandler);

        this.addEventListener(MouseEvent.MOUSE_WHEEL, mouseWheelHandler);
    }

    private function mouseWheelHandler(event:MouseEvent):void {

        var oldWidth:Number = this.width;
        var oldHeight:Number = this.height;
        var newScale:Number = this.scaleX + event.delta*0.01;
        if(newScale>scaleMax)
            newScale = scaleMax;
        if(newScale<scaleMin)
            newScale = scaleMin;
        this.scaleX = this.scaleY = newScale;
        this.x = int(((2 * mouseX) - (2 * (event.localX * this.scaleX))) * 0.5);
        this.y = int(((2 * mouseY) - (2 * (event.localY * this.scaleY))) * 0.5);
        //this.x += event.localX+(oldWidth - this.width)*0.5;
        //this.y += event.localY+(oldHeight - this.height)*0.5;

        /*TODO:remove
         if(scrollableContainer.width<viewPort.width){
         scrollableContainer.width = viewPort.width;
         scrollableContainer.scaleY = scrollableContainer.scaleX;
         }
         if(scrollableContainer.height<viewPort.height){
         scrollableContainer.height = viewPort.height;
         scrollableContainer.scaleX = scrollableContainer.scaleY;
         }
         trace('scrollableContainer.scaleX',scrollableContainer.scaleX);*/

        //TODO: double usage
        minX = -(this.width-viewPort.width);
        minY = -(this.height-viewPort.height);
        if(this.x > 0)
            this.x = 0;
        else if(this.x < minX)
            this.x = minX;

        if(this.y > 0)
            this.y = 0;
        else if(this.y < minY)
            this.y = minY;
    }

    private function mouseUpHandler(event:MouseEvent):void {
        mouseDown = false;
        stage.removeEventListener(Event.ENTER_FRAME, enterFrameHandler);
        checkClosePoints(event.stageX, event.stageY);
    }

    private function checkClosePoints(x:Number, y:Number):void {
        for each(var graphSprite:GraphSprite in graphSprites){
            graphSprite.checkClosePoints(x, y);
        }
    }

    private function mouseDownHandler(event:MouseEvent):void {
        mouseDown = true;
        stage.addEventListener(Event.ENTER_FRAME, enterFrameHandler);

        mouseDownPoint.x = event.stageX;
        mouseDownPoint.y = event.stageY;
        thisMouseDownPoint.x = this.x;
        thisMouseDownPoint.y = this.y;
    }

    private function enterFrameHandler(event:Event):void {
        if(mouseDown) {
            this.x = int(thisMouseDownPoint.x + (stage.mouseX - mouseDownPoint.x));
            this.y = int(thisMouseDownPoint.y + (stage.mouseY - mouseDownPoint.y));
        }

        /*if(scrollableContainer.x > 0)
         scrollableContainer.x = 0;
         else if(scrollableContainer.x < minX)
         scrollableContainer.x = minX;

         if(scrollableContainer.y > 0)
         scrollableContainer.y = 0;
         else if(scrollableContainer.y < minY)
         scrollableContainer.y = minY;*/
    }
}
}
