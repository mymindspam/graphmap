/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap.model {
    import flash.geom.Point;

    public class Rib {
        public var startPoint:Point;
        public var endPoint:Point;
        public function Rib(startPoint:Point, endPoint:Point) {
            this.startPoint = startPoint;
            this.endPoint = endPoint;
        }
    }
}
