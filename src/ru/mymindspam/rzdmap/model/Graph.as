/**
 * Created by oleg on 4/25/14.
 */
package ru.mymindspam.rzdmap.model {
public class Graph {
    public var id:String;
    public var color:uint = 0xFF0000;
    public var ribs:Vector.<Rib>;
    public function Graph(id:String, ribs:Vector.<Rib>, color:uint) {
        this.id = id;
        this.color = color;
        this.ribs = ribs;
    }
}
}
